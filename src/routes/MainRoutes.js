import React, { lazy } from 'react';

// project imports
import MainLayout from 'layout/MainLayout';
import Loadable from 'ui-component/Loadable';

// dashboard routing
const DashboardDefault = Loadable(lazy(() => import('views/dashboard/Default')));
const DashboardOrders = Loadable(lazy(() => import('views/dashboard/Orders')));
const DashboardWallet = Loadable(lazy(() => import('views/dashboard/Wallet')));
const DashboardAccount = Loadable(lazy(() => import('views/dashboard/account')));
const DashboardEarn = Loadable(lazy(() => import('views/dashboard/Earned')));
const DashboardRequest = Loadable(lazy(() => import('views/dashboard/Requests')));

// utilities routing
const UtilsTypography = Loadable(lazy(() => import('views/utilities/Typography')));
const UtilsColor = Loadable(lazy(() => import('views/utilities/Color')));
const UtilsShadow = Loadable(lazy(() => import('views/utilities/Shadow')));
const UtilsMaterialIcons = Loadable(lazy(() => import('views/utilities/MaterialIcons')));
const UtilsTablerIcons = Loadable(lazy(() => import('views/utilities/TablerIcons')));

const Products = Loadable(lazy(() => import('views/dashboard/Products')));

// sample page routing
const SamplePage = Loadable(lazy(() => import('views/sample-page')));

// ===========================|| MAIN ROUTING ||=========================== //

const MainRoutes = {
    path: '/',
    element: <MainLayout />,
    children: [
        {
            path: '/',
            element: <DashboardDefault />
        },
        {
            path: '/home',
            element: <DashboardDefault />
        },
        {
            path: '/request',
            element: <DashboardRequest />
        },
        {
            path: '/orders',
            element: <DashboardOrders />
        },
        {
            path: '/products',
            element: <Products />
        },
        {
            path: '/wallet',
            element: <DashboardWallet />
        },
        {
            path: '/account',
            element: <DashboardAccount />
        },
        {
            path: '/earn',
            element: <DashboardEarn />
        },
        {
            path: '/utils/util-typography',
            element: <UtilsTypography />
        },
        {
            path: '/utils/util-color',
            element: <UtilsColor />
        },
        {
            path: '/utils/util-shadow',
            element: <UtilsShadow />
        },
        {
            path: '/icons/tabler-icons',
            element: <UtilsTablerIcons />
        },
        {
            path: '/icons/material-icons',
            element: <UtilsMaterialIcons />
        },

        {
            path: '/sample-page',
            element: <SamplePage />
        }
    ]
};

export default MainRoutes;