import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import 'react-notifications/lib/notifications.css';

import { NotificationContainer, NotificationManager } from 'react-notifications';

// material-ui
import { makeStyles } from '@material-ui/styles';
import {
    Box,
    Button,
    FormControl,
    Grid,
    InputLabel,
    OutlinedInput,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper   
} from '@material-ui/core';

// third party
import { Formik } from 'formik';

// project imports
import AnimateButton from 'ui-component/extended/AnimateButton';

// assets

// style constant
const useStyles = makeStyles((theme) => ({
    redButton: {
        fontSize: '1rem',
        fontWeight: 500,
        backgroundColor: theme.palette.grey[50],
        border: '1px solid',
        borderColor: theme.palette.grey[100],
        color: theme.palette.grey[700],
        textTransform: 'none',
        '&:hover': {
            backgroundColor: theme.palette.primary.light
        },
        [theme.breakpoints.down('sm')]: {
            fontSize: '0.875rem'
        }
    },
    signDivider: {
        flexGrow: 1
    },
    signText: {
        cursor: 'unset',
        margin: theme.spacing(2),
        padding: '5px 56px',
        borderColor: `${theme.palette.grey[100]} !important`,
        color: `${theme.palette.grey[900]}!important`,
        fontWeight: 500
    },
    loginIcon: {
        marginRight: '16px',
        [theme.breakpoints.down('sm')]: {
            marginRight: '8px'
        }
    },
    loginInput: {
        ...theme.typography.customInput
    }
}));

//= ===========================|| FIREBASE - LOGIN ||============================//

const MpesaTopUp = () => {
    const classes = useStyles();
    const [prodName, setProdName] = useState('');
    const [prodDesc, setProdDesc] = useState('');
    const [prodPrice, setProdPrice] = useState('');
    const [time, setTime] = useState('');
    const [prodImage, setProdImage] = useState('');

    function createData(Product, Quantity, Price) {
        return { Product, Quantity, Price };
      }
    const rows = [
        createData('Frozen yoghurt', 159, 6.0),
        createData('Gucci Slippers', 'Just slippers', 345),
      ];
    async function TopUp() {
        const user = localStorage.getItem('user-info');
        const User = JSON.parse(user);
        const Data = new FormData();
        const product = {
            proId: undefined,
            proName: prodName,
            prAccId: User.usrAccId,
            proPrice: prodPrice,
            proStatus: '',
            proDesc: prodDesc,
            proCreatedAt: new Date(),
            proCreatedBy: User.usrId,
            proImageUrl: prodImage
        }

        Data.append('data', JSON.stringify(product));
        console.warn(Data);
        let result = await fetch(`http://165.227.58.99/usr/saveProduct.action?Authorization=${User.usrEncryptedPassword}`, {
            method: 'POST',
            body: JSON.stringify(product),
            headers: {
                'Content-Type': 'application/json;'
            }
        });
        result = await result.json();
        if (result.proId !== undefined || result.proId !== null) {
            NotificationManager.success('Product was saved successfully', 'Success');
        } else {
            NotificationManager.error('There was an problem while trying to process your request, kindly try again later', 'Failed');
        }
        console.log(JSON.stringify(result));
    };
    return (
        <>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                    <TableCell>Product</TableCell>
                    <TableCell align="right">Quantity</TableCell>
                    <TableCell align="right">Price</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row) => (
                    <TableRow
                        key={row.name}
                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                    >
                        <TableCell component="th" scope="row">
                        {row.name}
                        </TableCell>
                        <TableCell align="right">{row.calories}</TableCell>
                        <TableCell align="right">{row.fat}</TableCell>
                    </TableRow>
                    ))}
                </TableBody>
                </Table>
            </TableContainer>
            <Formik >
    
                <form>
                    <FormControl fullWidth className={classes.loginInput}>
                        <InputLabel htmlFor="outlined-adornment-email-login">Delivery Days</InputLabel>
                        <OutlinedInput
                            fullWidth
                            label="Phone Number"
                            margin="none"
                            name="usrMobileNumber"
                            type="text"
                            className={classes.loginInput}
                            value={prodName}
                            onChange={(e) => setProdName(e.target.value)}
                        />
                    </FormControl>

                    <FormControl fullWidth className={classes.loginInput}>
                        <InputLabel htmlFor="outlined-adornment-email-login">Description</InputLabel>
                        <OutlinedInput
                            fullWidth
                            label="Phone Number"
                            margin="none"
                            name="Amount"
                            type="integer"
                            className={classes.loginInput}
                            value={prodDesc}
                            onChange={(e) => setProdDesc(e.target.value)}
                        />
                    </FormControl>

                    <FormControl fullWidth className={classes.loginInput}>
                        <InputLabel htmlFor="outlined-adornment-email-login">Price</InputLabel>
                        <OutlinedInput
                            fullWidth
                            label="Phone Number"
                            margin="none"
                            name="Amount"
                            type="integer"
                            className={classes.loginInput}
                            value={prodPrice}
                            onChange={(e) => setProdPrice(e.target.value)}
                        />
                    </FormControl>

                    <FormControl fullWidth className={classes.loginInput}>
                        <InputLabel htmlFor="outlined-adornment-email-login">Delivery Timeline</InputLabel>
                        <OutlinedInput
                            fullWidth
                            label="Phone Number"
                            margin="none"
                            name="Amount"
                            type="integer"
                            className={classes.loginInput}
                            value={time}
                            onChange={(e) => setTime(e.target.value)}
                        />
                    </FormControl>

                    <FormControl fullWidth className={classes.loginInput}>
                        <InputLabel htmlFor="outlined-adornment-email-login">Image</InputLabel>
                        <OutlinedInput
                            fullWidth
                            label="Phone Number"
                            margin="none"
                            name="Amount"
                            type="file"
                            className={classes.loginInput}
                            value={prodImage}
                            onChange={(e) => setProdImage(e.target.value)}
                        />
                    </FormControl>

                    <Box
                        sx={{
                            mt: 2
                        }}
                    >
                        <AnimateButton>
                            <Button
                                disableElevation
                                fullWidth
                                size="small"
                                variant="contained"
                                color="primary"
                                onClick={() => TopUp()}
                            >
                                Add
                            </Button>
                        </AnimateButton>
                    </Box>
                </form>
            </Formik>
        </>
    );
};

export default MpesaTopUp;
