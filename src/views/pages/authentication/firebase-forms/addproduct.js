import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import 'react-notifications/lib/notifications.css';

import { NotificationContainer, NotificationManager } from 'react-notifications';

// material-ui
import { makeStyles } from '@material-ui/styles';
import {
    Box,
    Button,
    FormControl,
    InputLabel,
    OutlinedInput,
    Snackbar
} from '@material-ui/core';
import MuiAlert from '@material-ui/core/Alert';

// third party
import { Formik } from 'formik';

// project imports
import AnimateButton from 'ui-component/extended/AnimateButton';

// assets

// style constant
const useStyles = makeStyles((theme) => ({
    redButton: {
        fontSize: '1rem',
        fontWeight: 500,
        backgroundColor: theme.palette.grey[50],
        border: '1px solid',
        borderColor: theme.palette.grey[100],
        color: theme.palette.grey[700],
        textTransform: 'none',
        '&:hover': {
            backgroundColor: theme.palette.primary.light
        },
        [theme.breakpoints.down('sm')]: {
            fontSize: '0.875rem'
        }
    },
    signDivider: {
        flexGrow: 1
    },
    signText: {
        cursor: 'unset',
        margin: theme.spacing(2),
        padding: '5px 56px',
        borderColor: `${theme.palette.grey[100]} !important`,
        color: `${theme.palette.grey[900]}!important`,
        fontWeight: 500
    },
    loginIcon: {
        marginRight: '16px',
        [theme.breakpoints.down('sm')]: {
            marginRight: '8px'
        }
    },
    loginInput: {
        ...theme.typography.customInput
    }
}));


const Alert = React.forwardRef(function Alert(props, ref) {
    return <MuiAlert elevation={6} ref={ref} variant="filled" {...props} />;
});


const MpesaTopUp = () => {
    const classes = useStyles();
    const [prodName, setProdName] = useState('');
    const [prodDesc, setProdDesc] = useState('');
    const [prodPrice, setProdPrice] = useState('');
    const [time, setTime] = useState('');
    const [prodImage, setProdImage] = useState('');
    const [usrFileDetais, setFile] = useState('');
    const [imageName, setImageName] = useState('');
    const history = useNavigate();

    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        setOpen(true);
    };

    const handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        setOpen(false);
    };

    async function TopUp() {
        const user = localStorage.getItem('user-info');
        const User = JSON.parse(user);
        const Data = new FormData();
        const product = {
            proId: undefined,
            proName: prodName,
            prAccId: User.usrAccId,
            proPrice: prodPrice,
            proStatus: '',
            proDesc: prodDesc,
            proCreatedAt: new Date(),
            proCreatedBy: User.usrId,
            proImageUrl: `./myimages/OTP_IMAGES/ATTACHEMENTS/${imageName}`
        }

        Data.append('data', JSON.stringify(product));
        Data.append('file', usrFileDetais);
        console.warn(Data);
        let result = await fetch(`http://165.227.58.99/usr/saveProduct.action?Authorization=${User.usrEncryptedPassword}`, {
            method: 'POST',
            body: Data,

            headers: {
                //'Content-Type': 'multipart/form-data;'
            }
        });
        result = await result.json();
        window.location.reload();
        if (result.proId !== undefined || result.proId !== null) {
            NotificationManager.success('Product was saved successfully', 'Success');

        } else {
            NotificationManager.error('There was an problem while trying to process your request, kindly try again later', 'Failed');
        }
        console.log(JSON.stringify(result));
        //history('/products')
    };
    return (
        <>
            <Formik >
                <form>
                    <FormControl fullWidth className={classes.loginInput}>
                        <InputLabel htmlFor="outlined-adornment-email-login">Product Name</InputLabel>
                        <OutlinedInput
                            fullWidth
                            label="Phone Number"
                            margin="none"
                            name="usrMobileNumber"
                            type="text"
                            className={classes.loginInput}
                            value={prodName}
                            onChange={(e) => setProdName(e.target.value)}
                        />
                    </FormControl>

                    <FormControl fullWidth className={classes.loginInput}>
                        <InputLabel htmlFor="outlined-adornment-email-login">Description</InputLabel>
                        <OutlinedInput
                            fullWidth
                            label="Phone Number"
                            margin="none"
                            name="Amount"
                            type="integer"
                            className={classes.loginInput}
                            value={prodDesc}
                            onChange={(e) => setProdDesc(e.target.value)}
                        />
                    </FormControl>

                    <FormControl fullWidth className={classes.loginInput}>
                        <InputLabel htmlFor="outlined-adornment-email-login">Price</InputLabel>
                        <OutlinedInput
                            fullWidth
                            label="Phone Number"
                            margin="none"
                            name="Amount"
                            type="integer"
                            className={classes.loginInput}
                            value={prodPrice}
                            onChange={(e) => setProdPrice(e.target.value)}
                        />
                    </FormControl>

                    <FormControl fullWidth className={classes.loginInput}>
                        <InputLabel htmlFor="outlined-adornment-email-login">Delivery Timeline</InputLabel>
                        <OutlinedInput
                            fullWidth
                            label="Phone Number"
                            margin="none"
                            name="Amount"
                            type="integer"
                            className={classes.loginInput}
                            value={time}
                            onChange={(e) => setTime(e.target.value)}
                        />
                    </FormControl>

                    <FormControl fullWidth className={classes.loginInput}>
                        <InputLabel htmlFor="outlined-adornment-email-login">Image</InputLabel>
                        <OutlinedInput
                            fullWidth
                            label="Phone Number"
                            margin="none"
                            name="Amount"
                            type="file"
                            className={classes.loginInput}
                            value={prodImage}
                            onChange={(e) => {
                                setFile(e.target.files[0]);

                                setImageName(e.target.files[0].name);
                            }}
                        />
                    </FormControl>

                    <Box
                        sx={{
                            mt: 2
                        }}
                    >
                        <AnimateButton>
                            <Button
                                disableElevation
                                fullWidth
                                size="small"
                                variant="contained"
                                color="primary"
                                onClick={() => TopUp()}
                            >
                                Add
                            </Button>
                        </AnimateButton>
                    </Box>
                </form>
            </Formik>

            <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
                <Alert onClose={handleClose} severity="success" sx={{ width: '100%' }}>
                    Product Updated Successfully
                </Alert>
            </Snackbar>
        </>
    );
};

export default MpesaTopUp;
