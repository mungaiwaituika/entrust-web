import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';
import OtpInput from 'react-otp-input';

// material-ui
import {
    Box,
    Button,
    Checkbox,
    FormControl,
    FormControlLabel,
    FormHelperText,
    Grid,
    IconButton,
    InputAdornment,
    InputLabel,
    OutlinedInput,
    TextField,
    Typography,
    useMediaQuery
} from '@material-ui/core';
import { makeStyles } from '@material-ui/styles';

// style constant
const useStyles = makeStyles((theme) => ({
    redButton: {
        fontSize: '1rem',
        fontWeight: 500,
        backgroundColor: theme.palette.grey[50],
        border: '1px solid',
        borderColor: theme.palette.grey[100],
        color: theme.palette.grey[700],
        textTransform: 'none',
        '&:hover': {
            backgroundColor: theme.palette.primary.light
        },
        [theme.breakpoints.down('sm')]: {
            fontSize: '0.875rem'
        }
    },
    signDivider: {
        flexGrow: 1
    },
    signText: {
        cursor: 'unset',
        margin: theme.spacing(2),
        padding: '5px 56px',
        borderColor: `${theme.palette.grey[100]} !important`,
        color: `${theme.palette.grey[900]}!important`,
        fontWeight: 500
    },
    loginIcon: {
        marginRight: '16px',
        [theme.breakpoints.down('sm')]: {
            marginRight: '8px'
        }
    },
    loginInput: {
        ...theme.typography.customInput
    },
    submit: {
      margin: theme.spacing(3, 0, 2)
    },
}));

//= ===========================|| FIREBASE - OTP ||============================//

const OTP = () => {
    const classes = useStyles();
    const [OTP, setOTP] = useState('')
    const user = localStorage.getItem('user-state');
    const User = JSON.parse(user);
    const Data = new FormData();
    const OTPData = {
        usrSalt: OTP,
        usrId: User.usrId
    }
    Data.append('data', JSON.stringify(OTPData));
    console.warn(Data);
    const history = useNavigate();
    // useEffect(() => {
    //     if (localStorage.getItem('user-info')) {
    //         history('/');
    //     }
    // }, []);

    async function OTPsubmit() {
        let result = await fetch('http://165.227.58.99/usr/verifyUser.action', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
        });

        result = await result.json();

        history('/login');
    }
    return (
        <>
            <Grid item spacing={4}>
              <OtpInput
                value={OTP}
                separator={
                  <span>
                    <strong>-</strong>
                  </span>
                }
                inputStyle={{
                  width: "3rem",
                  height: "3rem",
                  margin: "0 1rem",
                  fontSize: "2rem",
                  borderRadius: 4,
                  border: "1px solid rgba(0,0,0,0.3)"
                }}
                isInputNum='true'
              />
            </Grid>
            <Grid item>
              <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={() => OTPsubmit()}

              >
                Verify
              </Button>
            </Grid>

        </>
    );
};

export default OTP;
