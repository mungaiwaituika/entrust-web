import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link, useNavigate } from 'react-router-dom';

// material-ui
import { makeStyles } from '@material-ui/styles';
import {
    Box,
    Button,
    FormControl,
    Grid,
    InputLabel,
    OutlinedInput,
    Typography
} from '@material-ui/core';

// third party
import { Formik } from 'formik';

// project imports
import AnimateButton from 'ui-component/extended/AnimateButton';

// assets

// style constant
const useStyles = makeStyles((theme) => ({
    redButton: {
        fontSize: '1rem',
        fontWeight: 500,
        backgroundColor: theme.palette.grey[50],
        border: '1px solid',
        borderColor: theme.palette.grey[100],
        color: theme.palette.grey[700],
        textTransform: 'none',
        '&:hover': {
            backgroundColor: theme.palette.primary.light
        },
        [theme.breakpoints.down('sm')]: {
            fontSize: '0.875rem'
        }
    },
    signDivider: {
        flexGrow: 1
    },
    signText: {
        cursor: 'unset',
        margin: theme.spacing(2),
        padding: '5px 56px',
        borderColor: `${theme.palette.grey[100]} !important`,
        color: `${theme.palette.grey[900]}!important`,
        fontWeight: 500
    },
    loginIcon: {
        marginRight: '16px',
        [theme.breakpoints.down('sm')]: {
            marginRight: '8px'
        }
    },
    loginInput: {
        ...theme.typography.customInput
    }
}));

//= ===========================|| FIREBASE - LOGIN ||============================//

const MpesaTopUp = () => {
    const classes = useStyles();
    const [usrPhoneNumber, setPhone] = useState('');
    const [Amount, setAmount] = useState('');
    const history = useNavigate();


    async function TopUp() {
        try {
            console.log(usrPhoneNumber, Amount);
            const user = localStorage.getItem('user-info');
            const User = JSON.parse(user);
            const data = { phone: usrPhoneNumber, amount: Amount };
            let result = await fetch(`http://165.227.58.99/usr/mpesaPushAlt.action?Authorization=${User.usrEncryptedPassword}&accountref=${User.usrAccId}&amount=${Amount}&phone=${usrPhoneNumber}`, {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json;'
                }
            });
            result = await result.json();
            console.log(JSON.stringify(result));
            history('/');
        } catch (e) {
            console.log(e);
        }
    };

    return (
        <>
            <Grid container direction="column" justifyContent="center" spacing={2}>
                <Grid item xs={12} container alignItems="center" justifyContent="center">
                    <Box
                        sx={{
                            mb: 2
                        }}
                    >
                        <Typography variant="subtitle1">Top Up with your Phone Number </Typography>
                    </Box>
                </Grid>
            </Grid>

            <Formik >
                <form>
                    <FormControl fullWidth className={classes.loginInput}>
                        <InputLabel htmlFor="outlined-adornment-email-login">Phone Number</InputLabel>
                        <OutlinedInput
                            fullWidth
                            label="Phone Number"
                            margin="none"
                            name="usrMobileNumber"
                            type="text"
                            className={classes.loginInput}
                            value={usrPhoneNumber}
                            onChange={(e) => setPhone(e.target.value)}
                        />
                    </FormControl>

                    <FormControl fullWidth className={classes.loginInput}>
                        <InputLabel htmlFor="outlined-adornment-email-login">Amount</InputLabel>
                        <OutlinedInput
                            fullWidth
                            label="Phone Number"
                            margin="none"
                            name="Amount"
                            type="integer"
                            className={classes.loginInput}
                            value={Amount}
                            onChange={(e) => setAmount(e.target.value)}
                        />
                    </FormControl>

                    <Box
                        sx={{
                            mt: 2
                        }}
                    >
                        <AnimateButton>
                            <Button
                                disableElevation
                                fullWidth
                                size="large"
                                variant="contained"
                                color="secondary"
                                onClick={() => TopUp()}
                                to="/"
                            >
                                Top Up
                            </Button>
                        </AnimateButton>
                    </Box>
                </form>
            </Formik>
        </>
    )
};

export default MpesaTopUp;
