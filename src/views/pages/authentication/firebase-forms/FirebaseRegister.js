import React, { useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';

// material-ui
import { makeStyles } from '@material-ui/styles';
import {
    Box,
    Button,
    Checkbox,
    FormControl,
    FormControlLabel,
    FormHelperText,
    Grid,
    IconButton,
    InputAdornment,
    InputLabel,
    OutlinedInput,
    TextField,
    Typography,
    useMediaQuery
} from '@material-ui/core';

// third party
import * as Yup from 'yup';
import { Formik } from 'formik';

// project imports
import useScriptRef from 'hooks/useScriptRef';
import AnimateButton from 'ui-component/extended/AnimateButton';
import { strengthColor, strengthIndicator } from 'utils/password-strength';

// assets
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

// style constant
const useStyles = makeStyles((theme) => ({
    redButton: {
        fontSize: '1rem',
        fontWeight: 500,
        backgroundColor: theme.palette.grey[50],
        border: '1px solid',
        borderColor: theme.palette.grey[100],
        color: theme.palette.grey[700],
        textTransform: 'none',
        '&:hover': {
            backgroundColor: theme.palette.primary.light
        },
        [theme.breakpoints.down('sm')]: {
            fontSize: '0.875rem'
        }
    },
    signDivider: {
        flexGrow: 1
    },
    signText: {
        cursor: 'unset',
        margin: theme.spacing(2),
        padding: '5px 56px',
        borderColor: `${theme.palette.grey[100]} !important`,
        color: `${theme.palette.grey[900]}!important`,
        fontWeight: 500
    },
    loginIcon: {
        marginRight: '16px',
        [theme.breakpoints.down('sm')]: {
            marginRight: '8px'
        }
    },
    loginInput: {
        ...theme.typography.customInput
    }
}));
//= ==========================|| FIREBASE - REGISTER ||===========================//

const FirebaseRegister = ({ ...others }) => {
    const classes = useStyles();
    const scriptedRef = useScriptRef();
    const matchDownSM = useMediaQuery((theme) => theme.breakpoints.down('sm'));
    const [showPassword, setShowPassword] = React.useState(false);
    const [checked, setChecked] = React.useState(true);
    const [usrFullNames, setFullname] = useState('');
    const [usrNationalId, setIDnumber] = useState('');
    const [usrEmail, setEmail] = useState('');
    const [usrMobileNumber, setPhone] = useState('');
    const [usrEncryptedPassword, setpassword] = useState('');
    const [usrFileDetais, setFile] = useState('');
    const [usrUsername, setUsername] = useState('');
    const [usrTradeName, setusrTradeName] = useState('');
    const [usrRegNo, setusrRegNo] = useState('');
    const [usrDirectorId, setusrDirectorId] = useState('');
    const history = useNavigate();

    const [strength, setStrength] = React.useState(0);
    const [level, setLevel] = React.useState('');

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleMouseDownPassword = (event) => {
        event.preventDefault();
    };

    const changePassword = (value) => {
        const temp = strengthIndicator(value);
        setStrength(temp);
        setLevel(strengthColor(temp));
    };

    useEffect(() => {
        changePassword('123456');
    }, []);

    async function signUp() {
        const Data = new FormData();
        const User = { usrFullNames, usrEncryptedPassword, usrMobileNumber, usrNationalId, usrEmail, usrUsername, usrTradeName, usrTradeName, usrRegNo, usrDirectorId };
        Data.append('data', JSON.stringify(User));
        Data.append('file', usrFileDetais);
        console.warn(Data);
        let result = await fetch('http://165.227.183.87:8080/ENTRUST/usr/createUser.action', {
            method: 'POST',
            body: Data,
            headers: {
                //  'Content-Type': 'multipart/form-data;'
            }
        });
        result = await result.json();
        localStorage.setItem('user-state', JSON.stringify(result));
        history('/otp');
    }
    return (
        <>
            <Grid container direction="column" justifyContent="center" spacing={2}>
                <Grid item xs={12} container alignItems="center" justifyContent="center">
                    <Box
                        sx={{
                            mb: 2
                        }}
                    >
                        <Typography variant="subtitle1">Sign up with Email address</Typography>
                    </Box>
                </Grid>
            </Grid>

            <Formik
                initialValues={{
                    usrFullNames: '',
                    usrNationalId: '',
                    usrMobileNumber: '',
                    usrEncryptedPassword: '',
                    usrFileDetais: '',
                    submit: null
                }}
                validationSchema={Yup.object().shape({
                    usrFullNames: Yup.string().required('Fullname is required'),
                    usrNationalId: Yup.string().required('ID Number is required'),
                    usrMobileNumber: Yup.string().required('Phone Number is required'),
                    usrEmail: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
                    usrEncryptedPassword: Yup.string().max(255).required('Password is required')
                })}
                onSubmit={async (values, { setErrors, setStatus, setSubmitting }) => {
                    try {
                        if (scriptedRef.current) {
                            setStatus({ success: true });
                            setSubmitting(false);
                        }
                    } catch (err) {
                        console.error(err);
                        if (scriptedRef.current) {
                            setStatus({ success: false });
                            setErrors({ submit: err.message });
                            setSubmitting(false);
                        }
                    }
                }}
            >
                {({ errors, handleBlur, handleChange, handleSubmit, touched }) => (
                    <form noValidate onSubmit={handleSubmit} {...others}>
                        <Grid container spacing={matchDownSM ? 0 : 2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    fullWidth
                                    label="Full Name"
                                    margin="normal"
                                    name="usrFullNames"
                                    type="text"
                                    className={classes.loginInput}
                                    value={usrFullNames}
                                    onChange={(e) => setFullname(e.target.value)}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    fullWidth
                                    label="Phone Number"
                                    margin="normal"
                                    name="usrMobileNumber"
                                    type="text"
                                    className={classes.loginInput}
                                    value={usrMobileNumber}
                                    onChange={(e) => {
                                        setUsername(e.target.value);
                                        setPhone(e.target.value);
                                    }}
                                />
                            </Grid>
                        </Grid>
                        <FormControl fullWidth error={Boolean(touched.idnumber && errors.idnumber)} className={classes.loginInput}>
                            <InputLabel htmlFor="outlined-adornment-phone-register">ID Number</InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-phone-register"
                                type="text"
                                value={usrNationalId}
                                name="usrNationalId"
                                onBlur={handleBlur}
                                onChange={(e) => {
                                    setIDnumber(e.target.value);
                                    handleChange(e);
                                }}
                                inputProps={{
                                    classes: {
                                        notchedOutline: classes.notchedOutline
                                    }
                                }}
                            />
                            {touched.idnumber && errors.idnumber && (
                                <FormHelperText error id="standard-weight-helper-text--register">
                                    {' '}
                                    {errors.idnumber}{' '}
                                </FormHelperText>
                            )}
                        </FormControl>
                        <FormControl fullWidth error={Boolean(touched.email && errors.email)} className={classes.loginInput}>
                            <InputLabel htmlFor="outlined-adornment-email-register">Email Address</InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-email-register"
                                type="email"
                                value={usrEmail}
                                name="usrEmail"
                                onBlur={handleBlur}
                                onChange={(e) => {
                                    setEmail(e.target.value);
                                    handleChange(e);
                                }}
                                inputProps={{
                                    classes: {
                                        notchedOutline: classes.notchedOutline
                                    }
                                }}
                            />
                            {touched.email && errors.email && (
                                <FormHelperText error id="standard-weight-helper-text--register">
                                    {' '}
                                    {errors.email}{' '}
                                </FormHelperText>
                            )}
                        </FormControl>

                        <FormControl fullWidth error={Boolean(touched.password && errors.password)} className={classes.loginInput}>
                            <InputLabel htmlFor="outlined-adornment-password-register">Password</InputLabel>
                            <OutlinedInput
                                id="outlined-adornment-password-register"
                                type={showPassword ? 'text' : 'password'}
                                value={usrEncryptedPassword}
                                name="usrEncryptedPassword"
                                label="Password"
                                onBlur={handleBlur}
                                onChange={(e) => {
                                    handleChange(e);
                                    changePassword(e.target.value);
                                    setpassword(e.target.value);
                                }}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={handleClickShowPassword}
                                            onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                            {showPassword ? <Visibility /> : <VisibilityOff />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                                inputProps={{
                                    classes: {
                                        notchedOutline: classes.notchedOutline
                                    }
                                }}
                            />
                            {touched.password && errors.password && (
                                <FormHelperText error id="standard-weight-helper-text-password-register">
                                    {' '}
                                    {errors.password}{' '}
                                </FormHelperText>
                            )}
                        </FormControl>
                        <FormControl fullWidth className={classes.loginInput}>
                            <InputLabel htmlFor="outlined-adornment-email-login">Trade Name</InputLabel>
                            <OutlinedInput
                                fullWidth
                                label="Phone Number"
                                margin="none"
                                name="usrMobileNumber"
                                type="text"
                                className={classes.loginInput}
                                value={usrTradeName}
                                onChange={(e) => setusrTradeName(e.target.value)}
                            />
                        </FormControl>

                        <FormControl fullWidth className={classes.loginInput}>
                            <InputLabel htmlFor="outlined-adornment-email-login">Registration Number</InputLabel>
                            <OutlinedInput
                                fullWidth
                                label="Phone Number"
                                margin="none"
                                name="usrMobileNumber"
                                type="text"
                                className={classes.loginInput}
                                value={usrRegNo}
                                onChange={(e) => setusrRegNo(e.target.value)}
                            />
                        </FormControl>


                        <FormControl fullWidth className={classes.loginInput}>
                            <InputLabel htmlFor="outlined-adornment-email-login">Director Id</InputLabel>
                            <OutlinedInput
                                fullWidth
                                label="Phone Number"
                                margin="none"
                                name="usrMobileNumber"
                                type="text"
                                className={classes.loginInput}
                                value={usrDirectorId}
                                onChange={(e) => setusrDirectorId(e.target.value)}
                            />
                        </FormControl>
                        <FormControl fullWidth error={Boolean(touched.file && errors.file)} classname={classes.loginInput}>
                            <OutlinedInput
                                id="outlined-adornment-file-register"
                                type="file"
                                name="usrFileDetais"
                                label="Registration Documents"
                                variant="standard"
                                onBlur={handleBlur}
                                onChange={(e) => {
                                    setFile(e.target.files[0]);
                                    console.log(e.target.files[1]);
                                }}
                                inputProps={{
                                    classes: {
                                        notchedOutline: classes.notchedOutline
                                    }
                                }}
                            />
                            {touched.file && errors.passwofilerd && (
                                <FormHelperText error id="standard-weight-helper-text-password-register">
                                    {' '}
                                    {errors.file}{' '}
                                </FormHelperText>
                            )}
                        </FormControl>

                        {strength !== 0 && (
                            <FormControl fullWidth>
                                <Box
                                    sx={{
                                        mb: 2
                                    }}
                                >
                                    <Grid container spacing={2} alignItems="center">
                                        <Grid item>
                                            <Box
                                                backgroundColor={level.color}
                                                sx={{
                                                    width: 85,
                                                    height: 8,
                                                    borderRadius: '7px'
                                                }}
                                            />
                                        </Grid>
                                        <Grid item>
                                            <Typography variant="subtitle1" fontSize="0.75rem">
                                                {level.label}
                                            </Typography>
                                        </Grid>
                                    </Grid>
                                </Box>
                            </FormControl>
                        )}

                        <Grid container alignItems="center" justifyContent="space-between">
                            <Grid item>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={checked}
                                            onChange={(event) => setChecked(event.target.checked)}
                                            name="checked"
                                            color="primary"
                                        />
                                    }
                                    label={
                                        <Typography variant="subtitle1">
                                            Agree with &nbsp;
                                            <Typography variant="subtitle1" component={Link} to="#">
                                                Terms & Condition.
                                            </Typography>checked
                                        </Typography>
                                    }
                                />
                            </Grid>
                        </Grid>
                        {errors.submit && (
                            <Box
                                sx={{
                                    mt: 3
                                }}
                            >
                                <FormHelperText error>{errors.submit}</FormHelperText>
                            </Box>
                        )}

                        <Box
                            sx={{
                                mt: 2
                            }}
                        >
                            <AnimateButton>
                                <Button fullWidth size="large" type="submit" variant="contained" color="primary" onClick={() => signUp()}>
                                    Sign up
                                </Button>
                            </AnimateButton>
                        </Box>
                    </form>
                )}
            </Formik>
        </>
    );
};

export default FirebaseRegister;
