import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/styles';
import { Avatar, Button, CardActions, CardContent, Checkbox, Typography, Card, CardMedia } from '@material-ui/core';
import BackgroungImg from '../../../assets/images/profile.jpg';
const useStyles = makeStyles((theme) => ({
    
  media: {
    height: "200px"
  },
  round: {
      top: 316,
      width: 90,
      height: 90,
      borderRadius: 45,
      marginLeft: 70,
      background: theme.palette.success.light,
      opacity: 1,

  },

}));

const Requests = () => {
    const classes = useStyles();
    const [isLoading, setLoading] = useState(true);
    useEffect(() => {
        setLoading(false);
    }, []);

    const [open, setOpen] = React.useState(false);
    const handleToggle = () => {
        setOpen((prevOpen) => !prevOpen);
    };
    const anchorRef = React.useRef(null);
    const prevOpen = React.useRef(open);
    const [fullNames, SetFullNames] = React.useState('Guest');
    const [Email, usrEmail] = React.useState('Guest');
    const [IDNumber, usrNationalId] = React.useState('Guest');
    const [Number, usrMobileNumber] = React.useState('Guest');
    React.useEffect(() => {
        if (prevOpen.current === true && open === false) {
            anchorRef.current.focus();
        }

        if (localStorage.getItem('user-info') !== undefined || localStorage.getItem('user-info') !== null) {
            const User = JSON.parse(localStorage.getItem('user-info'));
            SetFullNames(User.usrFullNames);
            usrEmail(User.usrEmail);
            usrNationalId(User.usrNationalId);
            usrMobileNumber(User.usrMobileNumber);
        }

        prevOpen.current = open;
    }, [open]);
    return (
            <Card className={classes.root}>
                <CardMedia
                    className={classes.media}
                    image= { BackgroungImg }
                    title="Background Image"
                />
                <CardContent>
                    <Typography gutterBottom variant="h2" component="h2">
                        {fullNames}
                    </Typography>
                    <Typography variant="subtitle2" color="primary" component="p">
                    {Email}
                    </Typography>
                    <Typography variant="body2" color="textSecondary" component="p">
                    {IDNumber}
                    </Typography>

                </CardContent>
            </Card>
    );
};

export default Requests;
