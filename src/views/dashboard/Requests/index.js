import React, { useEffect, useState } from 'react';

// material-ui
import { Grid } from '@material-ui/core';
import PopularCard from '../Default/PopularCard';
import TotalGrowthBarChart from '../Default/TotalGrowthBarChart';
import { gridSpacing } from 'store/constant';

const Requests = () => {
    const [isLoading, setLoading] = useState(true);

    const [Orders, setOrders] = React.useState("[]");

    async function getRequests() {
        let user = localStorage.getItem('user-info');
        const User = JSON.parse(user);
        const UserId = { "Authorization": User.usrEncryptedPassword, "walAccId": User.usrAccId, "limit": 20, "start": 0 }
        console.warn(User);
        let result = await fetch('http://165.227.58.99/usr/fetchOrders.action?Authorization=' + User.usrEncryptedPassword + "&orderAccId=" + User.usrAccId, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
            body: JSON.stringify(UserId)
        });
        result = await result.json();
        console.log(result);

        if (result.success) {
            setOrders(JSON.stringify(result.jsonData));
            setLoading(false);
        }
    }

    useEffect(() => {
        getRequests();
    }, []);

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={12} md={8}>
                        <TotalGrowthBarChart orders={Orders} isLoading={isLoading} />
                    </Grid>
                    <Grid item xs={12} md={4}>
                        <PopularCard orders={Orders} isLoading={isLoading} />
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Requests;
