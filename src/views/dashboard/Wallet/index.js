import React, { useEffect, useState } from 'react';

// material-ui
import { Grid } from '@material-ui/core';
import PopularCard from '../Default/PopularCard';
import TotalGrowthBarChart from './TotalGrowthBarChart';
import { gridSpacing } from 'store/constant';
import EarningCard from '../Default/EarningCard';


import TotalOrderLineChartCard from '../Default/TotalOrderLineChartCard';

const Requests = () => {
    const [isLoading, setLoading] = useState(true);
    const [Orders, setOrders] = React.useState('[]');

    const [CompletedCount, setCompletedCount] = React.useState('[]');

    const [TotalOrderValue, setTotalOrderValue] = React.useState('0.00 KES');

    async function getOrders() {
        const user = localStorage.getItem('user-info');
        const User = JSON.parse(user);
        
        const UserId = { Authorization: User.usrEncryptedPassword, walAccId: User.usrAccId, limit: 20, start: 0 };
        console.warn(User);
        let result = await fetch(
            `http://165.227.58.99/usr/fetchOrders.action?Authorization=${User.usrEncryptedPassword}&orderAccId=${User.usrAccId}`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                },
                body: JSON.stringify(UserId)
            }
        );
        result = await result.json();
        console.log(result);

        if (result.success) {
            setCompletedCount(result.jsonData.length);
            setOrders(JSON.stringify(result.jsonData));
            console.log(Orders);
            setLoading(false);
            setTotalOrderValue(0);
        }
    }

    useEffect(() => {
        setLoading(false);
    }, []);
    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
                <Grid container spacing={gridSpacing}>
                    <Grid item lg={4} md={6} sm={6} xs={12}>
                        <EarningCard isLoading={isLoading} />
                    </Grid>
                    <Grid item lg={4} md={6} sm={6} xs={12}>
                        <TotalOrderLineChartCard newValue={TotalOrderValue} isLoading={isLoading} />
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={12} md={8}>
                        <TotalGrowthBarChart orders={Orders} isLoading={isLoading} />
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Requests;
