import PropTypes from 'prop-types';
import React, { useState } from 'react';

// material-ui
import { makeStyles } from '@material-ui/styles';
import { Button, CardActions, CardContent, Drawer, Grid, Typography } from '@material-ui/core';

// project imports
import MainCard from 'ui-component/cards/MainCard';
import SkeletonPopularCard from 'ui-component/cards/Skeleton/PopularCard';
import { gridSpacing } from 'store/constant';
import Wrapper from './wrapper';

import useScriptRef from 'hooks/useScriptRef';

// assets
import ChevronRightOutlinedIcon from '@material-ui/icons/ChevronRightOutlined';

// style constant
const useStyles = makeStyles((theme) => ({
    cardAction: {
        padding: '10px',
        paddingTop: 0,
        justifyContent: 'center'
    },
    primaryLight: {
        color: theme.palette.primary[200],
        cursor: 'pointer'
    },
    divider: {
        marginTop: '12px',
        marginBottom: '12px'
    },
    avatarSuccess: {
        width: '16px',
        height: '16px',
        borderRadius: '5px',
        backgroundColor: theme.palette.success.light,
        color: theme.palette.success.dark,
        marginLeft: '15px'
    },
    successDark: {
        color: theme.palette.success.dark
    },
    avatarError: {
        width: '16px',
        height: '16px',
        borderRadius: '5px',
        backgroundColor: theme.palette.orange.light,
        color: theme.palette.orange.dark,
        marginLeft: '15px'
    },
    errorDark: {
        color: theme.palette.orange.dark
    }
}));

// ===========================|| DASHBOARD DEFAULT - POPULAR CARD ||=========================== //

const PopularCard = ({ isLoading }) => {
    const scriptedRef = useScriptRef();
    const classes = useStyles();
    const [open, setOpen] = React.useState(false);
    const [usrUsername, setPhone] = useState('');
    const [usrEncryptedPassword, setpassword] = useState('');
    const handleToggle = () => {
        setOpen(!open);
    };
    async function Mpesa() {
        const User = { usrUsername, usrEncryptedPassword };
        console.warn(User);
        let result = await fetch('http://165.227.183.87:8080/ENTRUST/usr/postAuthenticateUser.action', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
            body: JSON.stringify(User)
        });
        result = await result.json();
    }
    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const handleClose = () => {
        setAnchorEl(null);
    };

    return (
        <>
            {isLoading ? (
                <SkeletonPopularCard />
            ) : (
                <MainCard content={false}>
                    <CardContent>
                        <Grid container spacing={gridSpacing}>
                            <Grid item xs={12}>
                                <Grid container alignContent="center" justifyContent="space-between">
                                    <Grid item>
                                        <Typography variant="h4">Mpesa</Typography>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                    </CardContent>
                    <CardActions className={classes.cardAction}>
                        <Button onClick={handleToggle} size="large"  variant="contained" color="success" disableElevation>
                            Top Up with Mpesa
                            <ChevronRightOutlinedIcon />
                        </Button>

                        <Drawer
                            anchor="right"
                            onClose={handleToggle}
                            open={open}
                            PaperProps={{
                                sx: {
                                    width: 500
                                }
                            }}>
                            <Wrapper />
                        </Drawer>
                    </CardActions>
                </MainCard>
            )}
        </>
    );
};

PopularCard.propTypes = {
    isLoading: PropTypes.bool
};

export default PopularCard;
