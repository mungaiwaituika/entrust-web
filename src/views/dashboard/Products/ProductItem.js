import React, { useEffect, useState } from 'react';
// material-ui
import { makeStyles } from '@material-ui/styles';
import { TextField, Button, Dialog, Grid, DialogActions, DialogContent, Typography, Card, CardMedia, CardContent, DialogTitle, Checkbox } from '@material-ui/core';
import IconShare from 'assets/images/icons/share.svg';
import Artwork from 'assets/images/icons/artbard.svg';




// project imports

const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 380,
        margin: 10,
    },
    media: {
        height: "200px"
    }
}));

const ProductItem = ({ productItem }) => {
    const classes = useStyles();
    const [isLoading, setLoading] = useState(true);
    const [checked, setChecked] = React.useState(false);
  
    const handleChange = (event) => {
      setChecked(event.target.checked);
    };

    const [open, setOpen] = React.useState(false);

    const handleClickOpen = () => {
      setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
      };
    

    useEffect(() => {
        setLoading(true);
    }, []);
    return (
        <Card className={classes.root}>
            <CardMedia
                className={classes.media}
                image={productItem.proImageUrl}
                title="Product Image"
            />
            <CardContent>
                <Typography gutterBottom variant="h2" component="h2">
                    {productItem.proName}
                </Typography>
                <Grid container spacing={2}>
                    <Grid item xs={8}>
                        <Typography variant="subtitle2" color="primary" component="p">
                            {productItem.proPrice} KES
                        </Typography>
                        <Typography variant="body2" color="textSecondary" component="p">
                            {productItem.proDesc}
                        </Typography>
                        <Typography gutterBottom variant="subtitle1" color="primary" component="p">
                            Product Number: {productItem.proId}
                        </Typography>
                    </Grid>
                    <Grid item xs={2}>
                        <Button >
                            <img src={ IconShare } />
                        </Button>
                    </Grid>
                </Grid>

            </CardContent>
        </Card>
    );
};

export default ProductItem;
