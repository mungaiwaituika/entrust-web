import React, { useEffect, useState } from 'react';
// material-ui
import { makeStyles } from '@material-ui/styles';
import { Dialog, Button, Grid, Drawer, DialogActions, DialogContent, TextField, DialogTitle} from '@material-ui/core';

import ProductItem from './ProductItem';
import LocalMallOutlinedIcon from '@material-ui/icons/LocalMallOutlined';
import Wrapper from './wrapper';

const useStyles = makeStyles((theme) => ({
    root: {
        maxWidth: 300
    },
    media: {
        height: "200px"
    },
    avatar: {
        ...theme.typography.commonAvatar,
        ...theme.typography.largeAvatar,
        backgroundColor: theme.palette.primary[800],
        color: '#fff',
        marginTop: '8px'
    },
}));
let listItems;

const Products = () => {
    const classes = useStyles();
    const [isLoading, setLoading] = useState(true);
    const [Orders, setOrders] = React.useState('[]');
    const [open, setOpen] = React.useState(false);
    const [funguka, setFunguka] = React.useState(false);
    const [orderPhoneNumber, setorderPhoneNumber] = useState('');
    const [orderDeliveryAddress, setorderDeliveryAddress] = useState('');
    const [products, setproducts] = useState('');
    const [orderEmail, setorderEmail] = useState('');
    const message1 = 'Succesfully posted Product';

    const handleClickOpen = () => {
        setFunguka(true);
      };
      const handleClose = () => {
        setFunguka(false);
        };
      
  
    const handleToggle = () => {
        setOpen(!open);
    };
    async function getProducts() {
        const user = localStorage.getItem('user-info');
        const User = JSON.parse(user);
        const UserId = { Authorization: User.usrEncryptedPassword, walAccId: User.usrAccId, limit: 20, start: 0 };
        console.warn(User);


        
        let result = await fetch(
            `http://165.227.58.99/usr/fetchProducts.action?Authorization=${User.usrEncryptedPassword}&prAccId=${User.usrAccId}`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                },
                body: JSON.stringify(UserId)
            }
        );
        result = await result.json();
        console.log(result);

        if (result.success) {
            setOrders(JSON.stringify(result.jsonData));
            listItems = result.jsonData.map((prod) => (
                <Grid xs={12} sm={3} md={3} >
                    <ProductItem productItem={prod} isLoading={isLoading} />
                </Grid>
            ));
            setLoading(false);
        }
    }


    async function postOrder() {
        const user = localStorage.getItem('user-info');
        const User = JSON.parse(user);
        const Data = new FormData();
        const order = {
            orderStatus: "Active",
            orderDesc: "TRHTJJY",
            orderPhoneNumber: orderPhoneNumber,
            orderEmail: orderEmail,
            orderDeliveryAddress: orderDeliveryAddress
        }

        Data.append('data', JSON.stringify(order));

        let result = await fetch(
            `http://165.227.58.99/usr/fetchProducts.action?Authorization=${User.usrEncryptedPassword}&product=${products}`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                },
                body: JSON.stringify(order)
            }

        );
        result = await result.json();
        console.log(result);
    };

    getProducts();
    return (
        <>
            <Grid container spacing={0}>

                <Grid item xs={12}>

                    <Grid container justifyContent="space-between">
                        <Grid item>
                        </Grid>
                        <Grid item>
                            <Button
                                disableElevation
                                size="large"
                                onClick={handleToggle}
                            >
                                Add Products
                            </Button>
                            <Button
                                disableElevation
                                size="large"
                                onClick={handleClickOpen}
                            >
                                Send Requests
                            </Button>

                            <Drawer
                                anchor="right"
                                onClose={handleToggle}
                                open={open}
                                PaperProps={{
                                    sx: {
                                        width: 400
                                    }
                                }}
                            >
                                <Wrapper />
                            </Drawer>
                            <Dialog open={funguka} onClose={handleClose}>
                                <DialogTitle>Order Recipients Details</DialogTitle>
                                <DialogContent>
                                <TextField
                                    autoFocus   
                                    margin="dense"
                                    id="Integer"
                                    label="Phone Number"
                                    type="integer"
                                    fullWidth
                                    variant="standard"
                                    value={orderPhoneNumber}
                                    onChange={(e) => setorderPhoneNumber(e.target.value)}
                                />
                                <TextField
                                    margin="dense"
                                    id="Integer"
                                    label="Email Address"
                                    type="email"
                                    fullWidth
                                    variant="standard"
                                    value={orderEmail}
                                    onChange={(e) => setorderEmail(e.target.value)}
                                />
                                <TextField
                                    margin="dense"
                                    id="Integer"
                                    label="Delivery Address"
                                    type="text"
                                    fullWidth
                                    variant="standard"
                                    value={orderDeliveryAddress}
                                    onChange={(e) => setorderDeliveryAddress(e.target.value)}
                                />
                                <TextField
                                    margin="dense"
                                    id="Integer"
                                    label="Product number, separated with a comma"
                                    type="text"
                                    fullWidth
                                    variant="standard"
                                    value={products}
                                    onChange={(e) => setproducts(e.target.value)}
                                />
                                </DialogContent>
                                <DialogActions>
                                <Button onClick={postOrder()}>Send Order</Button>
                                </DialogActions>
                            </Dialog>
                        </Grid>
                    </Grid>
                    <Grid container alignContent="center" justifyContent="center">
                        
                    </Grid>
                </Grid>
                {listItems}
            </Grid>
        </>
    );
};
export default Products;
