import React, { useEffect, useState } from 'react';

// material-ui
import { Grid, Divider } from '@material-ui/core';
import TotalGrowthBarChart from './number';
import { gridSpacing } from 'store/constant';

const Requests = () => {
    const [isLoading, setLoading] = useState(true);
    useEffect(() => {
        setLoading(false);
    }, []);
    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={12} md={4}>
                        <TotalGrowthBarChart isLoading={isLoading} />
                    </Grid>
                    <Grid item xs={12} md={2}>
                    <Divider orientation="vertical" flexItem />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Requests;
