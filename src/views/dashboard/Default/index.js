import React, { useEffect, useState } from 'react';

// material-ui
import { Grid } from '@material-ui/core';

// project imports
import EarningCard from './EarningCard';
import PopularCard from './PopularCard';
import TotalOrderLineChartCard from './TotalOrderLineChartCard';
import TotalIncomeDarkCard from './TotalIncomeDarkCard';
import TotalIncomeLightCard from './TotalIncomeLightCard';
import TotalGrowthBarChart from './TotalGrowthBarChart';
import { gridSpacing } from 'store/constant';
import { Link, useNavigate } from 'react-router-dom';

// ===========================|| DEFAULT DASHBOARD ||=========================== //

const Dashboard = () => {
    const [isLoading, setLoading] = useState(true);
    const [Orders, setOrders] = React.useState('[]');

    const [CompletedCount, setCompletedCount] = React.useState('[]');

    const [userAvailable, setuserAvailable] = React.useState('');

    const [TotalOrderValue, setTotalOrderValue] = React.useState('[]');

    const history = useNavigate();

    async function getOrders() {
        const user = localStorage.getItem('user-info');
        const User = JSON.parse(user);
        const UserId = { Authorization: User.usrEncryptedPassword, walAccId: User.usrAccId, limit: 20, start: 0 };
        console.warn(User);
        let result = await fetch(
            `http://165.227.58.99/usr/fetchOrders.action?Authorization=${User.usrEncryptedPassword}&orderPhoneNumber=${User.usrMobileNumber}`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    Accept: 'application/json'
                },
                body: JSON.stringify(UserId)
            }
        );
        result = await result.json();
        console.log(result);

        if (result.success) {
            setCompletedCount(result.jsonData.length);
            setOrders(JSON.stringify(result.jsonData));
            console.log(Orders);
            setLoading(false);
            setTotalOrderValue(0.0);
        }
    }

    useEffect(() => {
        if (localStorage.getItem('user-info') === undefined || localStorage.getItem('user-info') === null) {
        } else {
            getOrders();
        }
    }, []);

    return (
        <Grid container spacing={gridSpacing}>
            <Grid item xs={12}>
                <Grid container spacing={gridSpacing}>
                    <Grid item lg={6} md={6} sm={6} xs={12}>
                        <EarningCard isLoading={isLoading} />
                    </Grid>
                    <Grid item lg={6} md={6} sm={6} xs={12}>
                        <TotalOrderLineChartCard newValue={TotalOrderValue} isLoading={isLoading} />
                    </Grid>
                </Grid>
            </Grid>
            <Grid item xs={12}>
                <Grid container spacing={gridSpacing}>
                    <Grid item xs={12} md={12}>
                        <TotalGrowthBarChart orders={Orders} isLoading={isLoading} />
                    </Grid>
                </Grid>
            </Grid>
        </Grid>
    );
};

export default Dashboard;
