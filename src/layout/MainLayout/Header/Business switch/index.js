import React, { useState }from 'react';
import { ToggleButton, ToggleButtonGroup } from "@material-ui/core";

export default function ColorToggleButton() {
  const [alignment, setAlignment] = useState("Buy");
  
    const handleChange = (event, newAlignment) => {
      setAlignment(newAlignment);
    };
  
    return (
      <ToggleButtonGroup    
        color="primary"
        exclusive
        onChange={handleChange}
        alignContent="center" 
        justifyContent="center"
      >
        <ToggleButton color="secondary" value={setAlignment}>
          Buy
        </ToggleButton>
        <ToggleButton value="android">Sell</ToggleButton>
      </ToggleButtonGroup>
    );
  }
  