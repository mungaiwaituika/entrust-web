import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';

// material-ui
import { makeStyles } from '@material-ui/styles';
import { Avatar, Box, ButtonBase } from '@material-ui/core';

// project imports
import LogoSection from '../LogoSection';
import Switch from './Business switch/index'
import ProfileSection from './ProfileSection';
import NotificationSection from './NotificationSection';

// assets
import { IconMenu2 } from '@tabler/icons';

// style constant
const useStyles = makeStyles((theme) => ({
    grow: {
        flexGrow: 1
    },
    headerAvatar: {
        ...theme.typography.commonAvatar,
        ...theme.typography.mediumAvatar,
        transition: 'all .2s ease-in-out',
        background: theme.palette.primary.light,
        color: theme.palette.grey[900],
        '&:hover': {
            background: theme.palette.primary.dark,
            color: theme.palette.secondary.light
        }
    },
    boxContainer: {
        width: '228px',
        display: 'flex',
        [theme.breakpoints.down('md')]: {
            width: 'auto'
        }
    }
}));

// ===========================|| MAIN NAVBAR / HEADER ||=========================== //

const Header = ({ handleLeftDrawerToggle }) => {
    const classes = useStyles();
    const [noficationList, setNoficationList] = React.useState('[]');

    async function getNotifications() {
        const user = localStorage.getItem('user-info');
        const User = JSON.parse(user);
        const UserId = { Authorization: User.usrEncryptedPassword, walAccId: User.usrAccId, limit: 20, start: 0 };
        console.warn(User);
        let result = await fetch(
            `http://165.227.58.99/usr/fetchNotification.action?Authorization=${User.usrEncryptedPassword}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Accept: 'application/json'
            },
            body: JSON.stringify(UserId)
        }
        );
        result = await result.json();
        setNoficationList(JSON.stringify(result.jsonData));
        console.log(result);
    }

    useEffect(() => {
        getNotifications();
    }, []);


    return (
        <>
            {/* logo & toggler button */}
            <div className={classes.boxContainer}>
                <Box component="span" sx={{ display: { xs: 'none', md: 'block' }, flexGrow: 1 }}>
                    <LogoSection />
                </Box>
                <ButtonBase sx={{ borderRadius: '12px', overflow: 'hidden' }}>
                    <Avatar variant="rounded" className={classes.headerAvatar} onClick={handleLeftDrawerToggle} color="inherit">
                        <IconMenu2 stroke={1.5} size="1.3rem" />
                    </Avatar>
                </ButtonBase>
            </div>

            {/* header search */}

            <div className={classes.grow} />
            <Switch />
            <div className={classes.grow} />

            {/* notification & profile */}

            <NotificationSection noficationList={noficationList} />
            <ProfileSection />
        </>
    );
};

Header.propTypes = {
    handleLeftDrawerToggle: PropTypes.func
};

export default Header;
