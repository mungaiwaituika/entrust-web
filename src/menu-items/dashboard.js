// assets
import { IconMedal, IconUser, IconClipboardList, IconWallet, IconBrandTelegram, IconDashboard, IconDeviceAnalytics, IconBuildingStore } from '@tabler/icons';

// constant
const icons = {
    IconDashboard,
    IconDeviceAnalytics,
    IconBrandTelegram,
    IconWallet,
    IconClipboardList,
    IconUser,
    IconMedal,
    IconBuildingStore,
};

// ===========================|| DASHBOARD MENU ITEMS ||=========================== //

const dashboard = {
    id: 'dashboard',
    type: 'group',
    children: [
        {
            id: 'default',
            title: 'Dashboard',
            type: 'item',
            url: '/',
            icon: icons.IconDashboard,
            breadcrumbs: false
        },
        {
            id: 'orders',
            title: 'My Payments',
            type: 'item',
            url: '/orders',
            icon: icons.IconClipboardList,
            breadcrumbs: false
        },
        {
            id: 'wallet',
            title: 'Wallet',
            type: 'item',
            url: '/wallet',
            icon: icons.IconWallet,
            breadcrumbs: false
        },
        {
            id: 'Products',
            title: 'Products',
            type: 'item',
            url: '/products',
            icon: icons.IconBuildingStore,
            breadcrumbs: false
        },
        {
            id: 'account',
            title: 'Account',
            type: 'item',
            url: '/account',
            icon: icons.IconUser,
            breadcrumbs: false
        },
        {
            id: 'earn',
            title: 'Earn Money',
            type: 'item',
            url: '/earn',
            icon: icons.IconMedal,
            breadcrumbs: false
        }
    ]
};

export default dashboard;
